#!/bin/sh

yum install git curl unzip -y
cd /opt/
curl -fsSL get.docker.com -o get-docker.sh
sh get-docker.sh
sudo usermod -aG docker vagrant
systemctl start docker
systemctl enable docker
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
git clone https://gitlab.com/ITOpsTube-Webinars/Docker-packer.git
cd /opt/Docker-packer
/usr/local/bin/docker-compose up
